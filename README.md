
# Deepfake Recognizer

Several algorithms are used for deepfake recognition and classification. 

There are 3 types of images:
 - `real` - real images
 - `face2face` - the entirety of a face was transferred
 - `?` - only the facial shapes, topology and expressions were transferred, preserving the colors and general look
